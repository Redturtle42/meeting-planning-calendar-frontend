module.exports = {
  env: {
    browser: true,
    es2021: true,
    node: true,
    es6: true,
  },
  extends: ["airbnb-base", "eslint:recommended"],
  parser: "babel-eslint",
  parserOptions: {
    ecmaVersion: 12,
    sourceType: "module",
  },
  rules: {
    semi: [2, "never"],
    printWidth: 120,
    quotes: ["error", "single", { avoidEscape: true }],
  },
};
