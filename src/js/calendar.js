import { DAYS, HOURS } from "./mockData";

//Create 2D array from DAYS(row) and HOURS(column)

class CalendarTable {
  constructor() {
    this.table = Array.from(HOURS, (hour) =>
      Array.from(
        Object.values(DAYS),
        (day) => new CalendarEvent("", null, day, hour)
      )
    );
  }
}

// Constructor for one event

class CalendarEvent {
  constructor(title, participants, day, hour) {
    this.title = title;
    this.participants = participants;
    this.day = day;
    this.hour = hour;
    this.id = this.day + this.hour;
  }

  getTitle() {
    return this.title;
  }
}

export { CalendarTable, CalendarEvent };
