import { participants, DAYS, HOURS } from "./mockData";
import "./service";
import "../style/style.scss";

const part = document.getElementById("participants");
for (let i = 0; i < participants.length; i++) {
  let options = document.createElement("option");
  options.value, (options.innerHTML = participants[i]);
  part.appendChild(options);
}

const days = document.getElementById("days");
const dys = Object.values(DAYS);
for (let i = 0; i < dys.length; i++) {
  let options = document.createElement("option");
  options.value, (options.innerHTML = dys[i]);
  days.appendChild(options);
}

const hour = document.getElementById("hours");
for (let i = 0; i < HOURS.length; i++) {
  let options = document.createElement("option");
  options.value, (options.innerHTML = HOURS[i]);
  hour.appendChild(options);
}

const members = document.getElementById("filterMembers");
for (let i = 0; i < participants.length; i++) {
  let options = document.createElement("option");
  options.value, (options.innerHTML = participants[i]);
  members.appendChild(options);
}
