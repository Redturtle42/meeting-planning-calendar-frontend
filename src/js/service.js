import { HOURS, DAYS } from "./mockData.js";
import TableBuilder from "table-builder";
import { CalendarTable, CalendarEvent } from "./calendar.js";

// Create Table
let data = new CalendarTable();

// Render Table
const render = (table = data.table) => {
  // Fill up the table with rows and columns
  const jsonData = table.map((row) => ({
    monday: row[0].title
      ? `<p class="tableData">${row[0].title} 
      <button class="removeItemBtn"  onclick="removeItem('${row[0].id}' )">&times;</button></p>`
      : "",
    tuesday: row[1].title
      ? `<p class="tableData">${row[1].title} <button class="removeItemBtn" onclick="removeItem('${row[1].id}' )">&times;</button></p>`
      : "",
    wednesday: row[2].title
      ? `<p class="tableData">${row[2].title} <button class="removeItemBtn" onclick="removeItem('${row[2].id}' )">&times;</button></p>`
      : "",
    thursday: row[3].title
      ? `<p class="tableData">${row[3].title} <button class="removeItemBtn" onclick="removeItem('${row[3].id}' )">&times;</button></p>`
      : "",
    friday: row[4].title
      ? `<p class="tableData">${row[4].title}<button class="removeItemBtn" onclick="removeItem('${row[4].id}' )">&times;</button></p>`
      : "",
  }));

  const newTbl = new TableBuilder({ class: "time-table" })
    .setHeaders(DAYS)
    .setVertHeaders("Time:", HOURS)
    .setData(jsonData)
    .render();

  document.getElementById("calendarTable").innerHTML = newTbl;
};

// Error message when time slot is reserved
const warningMsg = () => {
  document.getElementById("errorMsg").style.display = "none";
};

//Add new event
const addNewItem = () => {
  const title = document.getElementById("title").value;
  const participants = document.getElementById("participants").value;
  const day = document.getElementById("days").value;
  const hour = document.getElementById("hours").value;
  const add = new CalendarEvent(title, participants, day, hour);
  for (let i = 0; i < data.table.length; i++) {
    for (let j = 0; j < data.table[i].length; j++) {
      if (
        data.table[i][j].day == day &&
        data.table[i][j].hour == hour &&
        participants &&
        title
      ) {
        if (data.table[i][j].title) {
          //Error message, if event is exist on date
          document.getElementById("errorMsg").style.display = "block";
        } else {
          data.table[i][j] = add;
          break;
        }
      }
    }
  }

  //Dropdown menu by members set to default after add new item
  document.getElementById("filterMembers").value = "";

  render();

  // Clear modal's values after closing
  $("#myModal").on("hidden.bs.modal", function () {
    $(this).find("input,select").val("").end();
  });

  //Hidden error message after modal is pop up
  $("#myModal").on("show.bs.modal", function () {
    document.getElementById("errorMsg").style.display = "none";
  });
};

// Filter by participants
const filterByMembers = () => {
  let selectedMembers = document.getElementById("filterMembers").value;
  const copyTable = JSON.parse(JSON.stringify(data.table));
  if (selectedMembers) {
    copyTable.forEach((row) => {
      row.forEach((item) => {
        if (item.participants !== selectedMembers) {
          item.title = "";
        }
      });
    });
  }
  render(copyTable);
};

// Remove item
const removeItem = (id) => {
  data.table.forEach((row) => {
    row.forEach((item) => {
      if (id == item.id) {
        item.title = "";
        item.participants = null;
      }
    });
  });
  render();
};

window.addNewItem = addNewItem;
window.filterByMembers = filterByMembers;
window.render = render;
window.removeItem = removeItem;
window.warningMsg = warningMsg;
