const participants = [
  "Alpha, Beta, Gamma",
  "Delta, Epsilon, Zeta",
  "Eta, Theta, Iota",
];

const DAYS = {
  monday: "Monday",
  tuesday: "Tuesday",
  wednesday: "Wednesday",
  thursday: "Thursday",
  friday: "Friday",
};

const HOURS = [
  "10.00",
  "11.00",
  "12.00",
  "13.00",
  "14.00",
  "15.00",
  "16.00",
  "17.00",
  "18.00",
];

export { participants, DAYS, HOURS };
