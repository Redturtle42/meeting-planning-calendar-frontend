# Calendar

## _Simple meeting planning calendar_

This calendar was written for a meeting room in an office.

## Features

> - Display planned meetings by title
> - Filter meetings by participants
> - Add new meeting
> - Delete meeting
> - Warning if time slot is reserved for the specific meeting room

## Tech

Used technologies:

- Vanilla Javascript
- Npm
- Webpack.js
- Babel.js
- HTML5
- SASS
- Bootstrap

## Installation

```sh
git clone https://Redturtle42@bitbucket.org/Redturtle42/meeting-planning-calendar-frontend.git
npm install
npm start
```

🎈Have fun!!!🪁
